import React, { useState } from 'react';
import View_card from './view_card';
import View_table from './view_table';

const Page_home = () => {
    const[currView,SetCurrView]=useState(View_card);

    const showTable=()=>{
        SetCurrView(View_table)
    };
    const showCard=()=>{
        SetCurrView(View_card)
    };

  return (
    <>
    {/* <div className='page-container'> */}
        <div className='container_title package-header'>
            <div className='package'>Hot Property for Sale in New Zealand
                <div className="table_view_btn button">
                    <button className="btn-table-view btn" onClick={showTable}> Table View</button>
                    <button className="btn-card-view btn" onClick={showCard}> Card View</button>
                </div>
            </div>  
         </div>
            <div className='card_container package-container'>
            
                {/* <View_card /> */}
                {currView}

            </div>
    {/* </div> */}
    
    </>
    

  )
};

export default Page_home;