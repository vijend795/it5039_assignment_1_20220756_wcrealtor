import React from 'react';




const AboutUs = () => {
  return ( 
  
  <main className='aboutus-container page-all'>

    <div id="page-name-bar-heading">
        

        <p id="text">About WC Realtors</p>
        <p id="intro">
          <div>
          We SEE A BRIGHTER WAYSM forward for our clients, our people, our planet, and our communities. By combining innovative technology with world-renowned expertise, we’re unveiling opportunities that create a brighter future for all.
          </div></p>

      </div>
      <div id="about-us-text">About WC
      We SEE A BRIGHTER WAYSM forward for our clients, our people, our planet, and our communities. By combining innovative technology with world-renowned expertise, we’re unveiling opportunities that create a brighter future for all.
      <br></br>
      At JLL, we take pride in doing things differently. We see the built environment as a powerful medium with which to change the world for the better. By combining innovative technology and data intelligence with our world-renowned expertise, we’re able to unveil untapped opportunities for success.
      <br></br>
      We help buy, build, occupy and invest in a variety of assets including industrial, commercial, retail, residential and hotel real estate. From tech startups to global firms, our clients span industries including banking, energy, healthcare, law, life sciences, manufacturing, and technology.
      </div>
      
      <div>

      </div>
      <div className='aboutus_subhead'>
        <div className='aboutus_subhead_header_title'>
        SEE A BRIGHTER WAY
        
        </div> 
        <div className='aboutus_subhead_header_info'>
        There’s the conventional way of doing things. And then, there’s the JLL way. A more innovative, intelligent, and human way. Find out how you can SEE A BRIGHTER WAY with JLL.
        </div>  
      
      </div>
      
      <div className='aboutus_subhead'>
      <div  className='aboutus_subhead_header_info'>
      Find out how we are committed to helping our clients from across the globe to SEE A BRIGHTER WAY. Explore our annual report and latest corporation information. In addition, visit our Investor relations site.
      </div>
      <div className='aboutus_subhead_header_title'>
      
      
      Thinking beyond
      </div>
    
      </div> 

  </main>

  )
};

export default AboutUs;