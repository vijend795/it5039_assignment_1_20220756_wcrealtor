import React from 'react';
import Image from 'next/image';
import UnderProcess from './UnderProcess';
import Link from 'next/link';


const CardPackageTemplate = ({rdata}) => {
  
  return (
    <>

         <div className='card-container'>
                <div className='card-img-container'>
                    <Image className="card-img" src={rdata.img} alt='Destination Image' layout="fill" objectFit="fill" 
                   >
                    </Image>
                </div>
                <div className='card-detail'>
                    <div className='card-title'> {rdata.street_address}, {rdata.suburb},{rdata.city},{rdata.country}</div>
                    <div className='card-info'>
                        <div className='card-info card-date'> Area: {rdata.area} <br></br>
                                </div>
                        <div className='card-info '> Property Detail- {rdata.property_detail}</div>
                    </div>
                    <div className='card-price'>Price: {rdata.price} </div>
                    
                    <button className='btn-viewdetail' type="button" onClick={UnderProcess}> View Details !</button> 
                    
                </div>
            </div>
       
    
    </>
  );
};

export default CardPackageTemplate;