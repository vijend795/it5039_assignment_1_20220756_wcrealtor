
import react,{ useState } from "react";
import DataApi from "./DataApi";
import TableViewTemplate from "./template_table";


const View_table=()=>{
    const [currData,setCurrData] =useState(DataApi);
        // console.log({DataArray})
        // console.log({currData})

    return(
        <>
              
            <table className="table_view">
            <thead className="table_header">
                <tr className="table_row">
                    <td className="table_col col_id">ID</td>
                    <td className="table_col col_fname">Address</td>
                    <td className="table_col col_lname" >Area</td>
                    <td className="table_col col_city">Price</td>
                    <td className="table_col col_city">Property Detail</td>
                </tr>
            </thead>
            <tbody>    
                {
                    DataApi.map((t)=>(
                        <TableViewTemplate key={t.id} tdata={t} />
                    ))
                }
                    
                
            </tbody>
         </table>
            
         
        </>
    );  

};

export default View_table;