import React, { useState } from 'react'
import DataApi from './DataApi'
import CardPackageTemplate from './template_card';

const View_card = () => {
    const [currData,setCurrData]=useState(()=>DataApi);
  return (
    <>
    
        {

            currData.map((e)=>(
                <CardPackageTemplate keys={e.id} rdata={e} />
            ))
        }
    </>
    
  )
}

export default View_card