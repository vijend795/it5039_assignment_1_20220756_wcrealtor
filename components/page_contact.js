import React from 'react';




const ContactForm = () => {
  const [formStatus, setFormStatus] = React.useState('Send')
  const onSubmit = (e) => {
    e.preventDefault()
    setFormStatus('Submitting...')
    const { name, email, message } = e.target.elements
    let conFom = {
      name: name.value,
      email: email.value,
      message: message.value,
    }
    console.log(conFom)
  }
  return (
    <div className="contact-container container-design-all">
      <h2 className="contact-form-header">Contact Form</h2>
      <form className='contact-form'>
        <div className="form-container">
          <label className="form-label"  htmlFor="name">
            Name
          </label>
          <input className="form-control" placeholder='Please enter your name...' type="text" id="name" required />
        </div>
        <div className="form-container">
          <label className="form-label"  htmlFor="email">
            Email
          </label>
          <input className="form-control" placeholder='Please enter your email address...'  type="email" id="email" required />
        </div>
        <div className="form-container">
          <label className="form-label"  htmlFor="message">
            Message
          </label>
          <textarea className="form-control form-msg-area" placeholder='Send us a message!...' id="message" required />
        </div>
        <div className="form-container btn-form_submit">
        <button className="btn-danger" type="submit">
              {formStatus}
        </button>
        </div>
        
      </form>
      
           
      
        
    </div>
  )
}
export default ContactForm