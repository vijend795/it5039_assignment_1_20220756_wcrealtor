import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { FiAlignJustify } from "react-icons/fi";

const Navbar = () => {
  return (
   <>
   <div className='home_page'>
     <nav>
      <div className='navigation-bar'>
          <Link href="/" className='header-menu logo'>
            <Image className='company-logo' src="/logo-bg.png" alt='Logo' width="150" height="100" />
            <div className='logo-name'>
                  WC Realtor 
              </div>  
          </Link>
          
      </div>
      <div className='navigation-menu-bar'>
        <div className='menu-icon'> <FiAlignJustify /></div>
            <Link href="/" className='header-menu menu-bar home-page' > Home</Link>
            <Link href="/aboutus" className='header-menu  menu-bar aboutus-page'> About Us</Link>
            <Link href="/search" className='header-menu  menu-bar search-page'> Search</Link>
            <Link href="/contact" className='header-menu  menu-bar contactus-page'> Contact Us</Link>
            <Link href="/login" className='header-menu menu-bar login-page'>Log in </Link>
    
      </div>
   </nav>
   </div>
   </>
  )
};

export default Navbar;