const TableViewTemplate=({tdata})=>{
  return (
      
      <tr className="table_row">
          <td className="table_col col_id">{tdata.id} </td>
          <td className="table_col col_address">{tdata.street_address}, {tdata.suburb}, {tdata.city}, {tdata.country} </td>
          <td className="table_col col_area">{tdata.area}</td>
          <td className="table_col col_price">{tdata.price}</td>
          <td className="table_col col_detail">{tdata.property_detail}</td>
      </tr>
  
  
  );
};

export default TableViewTemplate;