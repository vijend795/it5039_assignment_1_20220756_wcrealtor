import { userAgent } from 'next/server';
import React, { useState } from 'react';
import CardPackageTemplate from './template_card';

import dataApi from './dataApi';




const SearchBar = () => {
 
const [street_address,setStreet_address]=useState("");
const [suburb,setSuburb]=useState("");
const [city,setCity]=useState("");
const [property_detail,setProperty_detail]=useState("");




const Change_StreetAddress=(e)=>{
  setStreet_address(e.target.value)
};
const Change_Suburb=(e)=>{
  setSuburb(e.target.value)
};
const Change_City=(e)=>{
  setCity(e.target.value)
};

const Change_Property_detail=(e)=>{
  setProperty_detail(e.target.value)
};


const search=(data)=>{
  return data.filter(
  (item)=>

  item.suburb.toLowerCase().includes(suburb.toLowerCase()) &&
  item.street_address.toLowerCase().includes(street_address.toLowerCase()) &&
  item.property_detail.toLowerCase().includes(property_detail.toLowerCase()) &&
  item.city.toLowerCase().includes(city.toLowerCase())
  )
};



const reset_btn=()=>{
  return(

    setStreet_address(""),
    setSuburb(""),
      setCity(""),
      setProperty_detail("")
  
  )
  
}



  return (
    <>
    
     <div className='serachbar-container'>
     
      <div className='searchbar-subContainer'>
      <div>
        <div className='searchbar searchbar-header'>
          
          Search Property across New Zealand

        </div>
   
        <div className='searchbar-infobar'>
          <div className='subsearchbar searchbar-subinfo1'>
            <ul>Street Address <input type="text" placeholder='Search Street Address...' className='select-departure'
            onChange={Change_StreetAddress} value={street_address}/> </ul>
            <ul> Suburb <input type="text" placeholder='Search Suburb' className='select-destination' 
            onChange={Change_Suburb} value={suburb}/> </ul>
            <ul> City<input type="text" placeholder='Search City ...' className='date-selection'
            onChange={Change_City} value={city}/> </ul>  
            
            <ul>Property Detail<input type="text" placeholder='Search Property Detail ....' className='passenger-detail' onChange={Change_Property_detail} value={property_detail} /> </ul>  
            
          </div>
          
        </div>
        <div className='searchpage_reset_btn'>
        <button className='btn-reset' onClick={reset_btn} name="trip_Type" value="">Reset
        </button>
        </div>
        
      </div>
      </div>
     </div>
     
  
     <div className='package-header'>
            <div className='package'>
            Properties across New Zealand !!!
            </div> 
      
      </div>
        
      <div className='package-container'>  
       
            {
                search(dataApi).map((rdata)=>(
    
                  <CardPackageTemplate key={rdata.id} rdata={rdata} />
                ))

            }

       </div>
   </>
  );
};

export default SearchBar;