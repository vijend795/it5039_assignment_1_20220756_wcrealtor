import React from 'react';


const LogInForm = () => {

  return (
    <>
    <div className="login-container container-design-all">
      <div className="login-form-header">
          <div className='login-alreadymember'> Member Login</div>
          <div className='login-needhelp'> Need Help ?</div>
      </div>
      <form className='login-form'>
          <input className="login-username" placeholder='User name' type="text" id="username" required />
          <input className="login-password" placeholder='Password'  type="password" id="password" required />  
          <button className="btn-login" type="submit"> LogIn
            </button> 
        
        <div className='resister_user_title'>
            Don't have an account yet ?
        </div>
        <div className='resister_user_link'>
            Create an account
        </div>
        
      </form> 
    </div>
    </>
  )
}
export default LogInForm;