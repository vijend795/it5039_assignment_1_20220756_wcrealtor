import React from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import LogInForm from '../components/page_login';


const login = () => {
  return (
    <>
     <main className='login-page page-all'>
    <Navbar />
    <LogInForm />
    <Footer />
    </main>
    </>
    
    
    
  )
};

export default login;