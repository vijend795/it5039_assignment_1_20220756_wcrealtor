
import React from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import Page_search from '../components/page_search'



const search = () => {
  return (
    <>
        <Navbar />
        <Page_search />
        <Footer />
    
    </>
  )
}

export default search