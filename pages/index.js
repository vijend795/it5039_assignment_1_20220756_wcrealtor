import React, { useState } from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import styles from '../styles/index.module.css';
import Page_home from '../components/page_home';



 

const index = () => {
 


  return (
    <main className='index-page page-all'>
    <Navbar />

    <Page_home />
    <Footer />
    
    </main>
    
  );
};

export default index;