import React from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import AboutUs from '../components/page_aboutUs';


const aboutus = () => {
  return (
    <main className='index-page page-all'>
        <Navbar />
         <AboutUs />
         <Footer />
    </main>
    
  )
};

export default aboutus;