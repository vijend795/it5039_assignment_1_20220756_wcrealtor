import React from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import ContactForm from '../components/page_contact';

const home = () => {
  return (
    <main className='contact-page page-all'>
     
      <Navbar />
      <ContactForm />
      <Footer />
    
    </main>
   
  )
};

export default home;